import { Controller, Get, Body, Query } from '@nestjs/common';
import { HistoryService } from './history.service';
import { ApiBearerAuth } from "@nestjs/swagger";

export interface characterData{
  race: string
  genre: string
  classe: string
  metier: string
}
@Controller('api/url-api-description/v1/')
export class HistoryController {
  constructor(private chatGPTService: HistoryService) {}

  @ApiBearerAuth()
  @Get()
  async generateHistory(@Query() body: characterData) {
    return this.chatGPTService.generateText(body)
  }
}
