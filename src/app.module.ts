import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';

import { HistoryModule } from './history/history.module';
import { AdminMiddleware } from './middlewares/admin.middleware';
import { ConfigModule } from '@nestjs/config';
import { FirebaseAdminService } from './service/firebase-admin.service';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    HistoryModule,
  ],
  controllers: [],
  providers: [FirebaseAdminService],
})

export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(AdminMiddleware).forRoutes('api/url-api-description/v1/');
  }
}
